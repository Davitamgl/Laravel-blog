
<div style="display:flex; align-items: center">

  <h1 style="position:relative; top: -6px" >Laravel 8 From Scratch Blog Application</h1>
 
</div>

   !["CI / CD"](./README_FILES/application.png)


### Table of Contents
* [Prerequisites](#prerequisites)
* [Getting Started](#getting-started)
* [Database Diagram](#database-diagram)
* [Migrations](#migration)
* [Development](#development)


#
### Prerequisites

* [composer@2.1.9](https://getcomposer.org/)
* [php@8.0.11](https://www.php.net/)
* [npm@8.1.0](https://nodejs.org/en/download/)






### Getting Started
1\. First of all you need to clone Laravel 8 From Scratch repository from github:
```sh
git clone https://github.com/RedberryInternship/davitamaglobeli-laravel-8-from-scratch.git
```

2\. Next step requires you to run *composer install* in order to install all the dependencies.
```sh
composer install
```

3\. after you have installed all the PHP dependencies, it's time to install all the JS dependencies:
```sh
npm install
```

and also:
```sh
npm run dev
```
in order to build your JS/SaaS resources.

4\. Now we need to set our env file. Go to the root of your project and execute this command.
```sh
cp .env.example .env
```
And now you should provide **.env** file all the necessary environment variables:

#
**MYSQL:**
>DB_CONNECTION=mysql

>DB_HOST=127.0.0.1

>DB_PORT=3306

>DB_DATABASE=*****

>DB_USERNAME=*****

>DB_PASSWORD=*****


#


after setting up **.env** file, execute:
```sh
php artisan config:cache
```
in order to cache environment variables.

4\. Now execute in the root of you project following:
```sh
  php artisan key:generate
```
Which generates auth key.



##### Now, you should be good to go!

#
### Database Diagram
[Database Design Diagram](https://drawsql.app/redberry-12/diagrams/laravel-8-from-scratch)

!["CI / CD"](./README_FILES/drawsql.png)


#
### Migration
if you've completed getting started section, then migrating database if fairly simple process, just execute:
```sh
php artisan migrate
```

Run Seeder to generate some gibberish  data

```sh
php artisan migrate:fresh -seed
```

### Development

You can run Laravel's built-in development server by executing:

```sh
  php artisan serve
```

 you may run:

```sh
  npm run dev
```
to build your talwind css







