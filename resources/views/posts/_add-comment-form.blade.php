@auth
    <form method="POST" action="{{ route('comments.store', [$post->slug]) }}" class="border border-gray-200 p-6 rounded-xl">
        @csrf
        <header class="flex items-center">
            <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="" width="40" height="60" class="rounded-full">

            <h2 class="ml-4">Want To Participate?</h2>
        </header>
        <div class="mt-6">
            <textarea name="body" class="w-full text-sm focus:outline-none focus:ring" rows="5"
                placeholder="Quick, thing of something to say!" required></textarea>
            @error('body')
                <span class="text-xs text-red-500">{{ $message }}</span>
            @enderror
        </div>
        <div class="flex justify-end mt-6 pt-6 border-t border-gray-200">
            <x-submit-button>Post</x-submit-button>
        </div>
    </form>
@else
    <p class="font-semibold">
        <a href="{{ route('register.create') }}" class="hover:underline">Register</a> or
        <a href="{{ route('login.create') }}" class="hover:underline">Log in</a> to
        Leave a Comment.
    </p>
@endauth
