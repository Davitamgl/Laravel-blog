<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Category;
use App\Models\Post;

class AdminPostController extends Controller
{
	public function index()
	{
		return view('admin.posts.index', [
			'posts' => Post::paginate(50),
		]);
	}

	public function create()
	{
		return view('admin.posts.create', [
			'categories' => Category::all(),
		]);
	}

	public function edit(Post $post)
	{
		return view('admin.posts.edit', [
			'post'       => $post,
			'categories' => Category::all(),
		]);
	}

	public function store(StorePostRequest $request)
	{
		$attributes = $request->validated();
		$attributes['user_id'] = auth()->id();
		$attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');

		Post::create($attributes);

		return  redirect()->route('admin.posts.index')->with('success', 'Post Added!');
	}

	public function update(Post $post, StorePostRequest $request)
	{
		$attributes = $request->validated();

		if ($attributes['thumbnail'] ?? false)
		{
			$attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
		}

		$post->update($attributes);

		return redirect()->route('admin.posts.index')->with('success', 'Post Updated!');
	}

	public function destroy(Post $post)
	{
		$post->delete();

		return back()->with('success', 'Post Deleted!');
	}
}
