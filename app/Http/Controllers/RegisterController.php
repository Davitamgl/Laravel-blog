<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
	public function create()
	{
		return view('register.create');
	}

	public function store(StoreUserRequest $request)
	{
		$attributes = $request->validated();
		$user = User::create($attributes);
		Auth::login($user);

		return redirect('/')->with('success', 'Your account has been created.');
	}
}
