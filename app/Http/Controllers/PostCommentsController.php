<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostCommentsRequest;
use App\Models\Post;

class PostCommentsController extends Controller
{
	public function store(Post $post, StorePostCommentsRequest $request)
	{
		$attributes = $request->validated();

		$post->comments()->create([
			'user_id' => request()->user()->id,
			'body'    => $attributes['body'],
		]);
		return back();
	}
}
