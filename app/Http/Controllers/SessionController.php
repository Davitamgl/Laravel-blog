<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSessionRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SessionController extends Controller
{
	public function create()
	{
		return view('sessions.create');
	}

	public function store(StoreSessionRequest $request)
	{
		$attributes = $request->validated();

		if (Auth::attempt($attributes))
		{
			session()->regenerate();
			return redirect('/')->with('success', 'Welcome Back!');
		}

		throw ValidationException::withMessages([
			'email' => 'Your provided credentials could not be verofied',
		]);
	}

	public function destroy()
	{
		Auth::logout();
		return redirect('/')->with('success', 'Goodbye!');
	}
}
