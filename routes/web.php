<?php

use App\Http\Controllers\AdminPostController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\PostCommentsController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Route;

//HomePage
Route::get('/', [PostController::class, 'index'])->name('home');
Route::get('posts/{post:slug}', [PostController::class, 'show'])->name('home.show');

//Register
Route::get('register', [RegisterController::class, 'create'])->middleware('guest')->name('register.create');
Route::post('register', [RegisterController::class, 'store'])->middleware('guest')->name('register.store');

//Login
Route::get('login', [SessionController::class, 'create'])->middleware('guest')->name('login.create');
Route::post('login', [SessionController::class, 'store'])->middleware('guest')->name('login.store');
Route::post('logout', [SessionController::class, 'destroy'])->middleware('auth')->name('login.destroy');

//Comments
Route::post('posts/{post:slug}/comments', [PostCommentsController::class, 'store'])->name('comments.store');

//Newsletter
Route::post('newsletter', NewsletterController::class)->name('newsletter.store');

//Admin
Route::middleware('can:admin')->name('admin.')->group(function () {
	Route::resource('admin/posts', AdminPostController::class)->except('show');
});
